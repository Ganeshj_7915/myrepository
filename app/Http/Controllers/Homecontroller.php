<?php

namespace App\Http\Controllers;
use App\datas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

class Homecontroller extends Controller
{
    public function getHome() {
		$user_get = Auth::user();
		return view('home');
	}
}
