<?php

namespace App\Http\Controllers;

use App\data;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class articlecontroller extends Controller
{
	public function index()  {
		$data = data::latest("updated_at")->where("updated_at","<=","Carbon::now()")->get();
		return view("data")->with("d",$data);
	}
	public function show($id)  {
		$data = data::findOrFail($id);
		
		return view("show",compact("data"));
		
	}
	public function create() {
		return view('create');
	}
	
	public function display(Requests\createarticle $request) {
		
		$input["username"] = "karthik";
		//$input = Request::all();
		//$input["updated_at"] = Carbon::now();
		
		data::create($request->all());
		
		return  redirect("articles");
	}
	
	public function edit($id) {
		
		$article = data::findOrFail($id);
		
		return view("edit",compact("article"));
	}

	public function update($id, Request $request) {

		$article = data::findOrFail($id);

		$article->update($request->all());

		return redirect('articles');

}
}
