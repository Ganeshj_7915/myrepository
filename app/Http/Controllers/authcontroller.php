<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Input;
use App\user;
use App\profile;
use Hash;
use Carbon\Carbon;

class authcontroller extends Controller
{
    public function signupmethod() {
		$signup = array(
			'username' => 'required|max:20',
			'email' => 'required|email',
			'password' => 'required|max:20',
		);

	$validation = Validator::make(Input::all(),$signup);
	
	if($validation->fails()){
		return Redirect::to('/test')->with('error',$validation->errors()->first());
	}
	else{
		$user = new user;
		$user->name = Input::get('name');
		$user->username = Input::get('username');
		$user->password = Hash::make(Input::get('password'));
		$user->email = Input::get('email');		
		$user->updated_at = Carbon::now();
		$user->created_at = Carbon::now();	
	$users  = array(
	'name' => $user->username,
	'email' => $user->email);
	
	Mail::send('mailers', ['user' => $user], function($message) use ($user)
{	
    $message->from('ganeshjayaram97@gmail.com','Ganesh');
	$message->to($user->email, $user->username)->subject('Welcome to my Bhoologam :p !');
}); 
	$user->save();
	return view('profile',compact("user"));
	
	//return Redirect::to('/test')->with('error','success');
	}
}

	
	public function signinmethod() {
//		$this->validate($request,user::$login_validation);
	//	$get = Input::only('username','password');
		if(Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')))){
			return Redirect::intended('/home');
		}
		if(Auth::check()){
			echo "authorized one";
		}
		//echo $pass;
		return Redirect::to('/test');
	}
	
	public function logoutmethod() {
		Auth::logout();
		return Redirect::to('/test');
}

}