<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\datas;
use Input;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

class blogcontroller extends Controller
{
  public function newblog() {
	  return view("newblog");
  }
  
  public function postblogmethod() {
	  $blog = new datas;
	  $blog->title = Input::get('title');
	  $blog->body = Input::get('content');
	  $blog->writerid = Auth::user()->id;
	  $blog->username = Auth::user()->username;
	  $blog->save();
	  return view("postedpage",compact("blog"));
  }
  
  public function readblog($bid) {
	  $reads = datas::find($bid);
	  return view("read",compact('reads'));
  }
  
  public function editblog($bid){
	  $edit = datas::find($bid);
	  return view("editpost",compact("edit"));
  }
  
  public function deleteblog($bid) {
	  $delete = datas::find($bid);
	  return view("deletedblog",compact("delete"));
  }
  public function delblog($bid) {
	  $del = datas::find($bid);
	  $del->delete();
	  return view("delpost");
  }
   public function posteditblog() {
	   $bid = Input::get("edit");
	  $blog = datas::find($bid);
	  $blog->title = Input::get('title');
	  $blog->body = Input::get('content');
	  $blog->writerid = Auth::user()->id;
	  $blog->username = Auth::user()->username;
	  $blog->save();
	  return view("postedpage",compact("blog"));
  }
  
}
