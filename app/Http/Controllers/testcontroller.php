<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Config;
use App\datas;
use Carbon\Carbon;
use App\user;
use App\qs1;
use App\marks;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Validator;
use Input;
use Hash;
use App\Http\Requests;

class testcontroller extends Controller
{
public function Home() {
		if(Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')))){
			$getuser = Auth::user()->id;
			//$user = DB::table("marks")->select("count(*)")->where("userid","=",$getuser);
			$getmarks = new marks;
			$getmarks->userid = $getuser;
			$getmarks->created_at = Carbon::now();
			$getmarks->save();
			return view('/testhome');
		}
		/*
		if(Auth::check()){
			echo "authorized one";
		} 
		*/
		//echo $pass;
		
		return Redirect::to('/test1');

}
    public function testqs($id) {
		//$s = (($id-1)*5)+1 ;
		//$s1 = ($id)*5 ;
		//echo $id."  ".$s."  ".$s1;
		$get =  DB::table("qs1")->where('id', $id)->get();
		return view("tests1",compact("get"));
	}

public function goadmin() {
//		$this->validate($request,user::$login_validation);
	//	$get = Input::only('username','password');
		if(Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')))){
			return view('/admin-home');
		}
		if(Auth::check()){
			echo "authorized one";
		}
		//echo $pass;
		return Redirect::to('/test1');

}
	
    public function questions() {
		return view("questions");
	}

	public function submit(){
		$getmarks = new marks;
		$ans = Input::get("uans");
		$id = Input::get("testid");
		$get = DB::table("qs1")->select("crt")->where("id","=",$id)->get();
		//dd($get[0]);
		$getuser = Auth::user()->id;
		if($ans == $get[0]->crt){
			$update = DB::table('marks')->where('userid','=',$getuser)->increment('mark');
			//echo $update;
			//echo "correct";
		}
		$id = $id+1;
		//echo $id,$ans;
		//echo $get;
		$count = qs1::count();
		if($id == $count)
		{
			$get =  DB::table("qs1")->where('id', $id)->get();
			return view("finalpage",compact("get"));
		}
		else{
		$url =  "testqs/".$id;
		return Redirect::to($url);
		}
	}
	
	public function testmark() {
			$count = qs1::count();
			$id = Auth::user()->id;
			$total = DB::table('marks')->select('*')->where('userid','=',$id)->get();
			$avg =  ($total[0]->mark / $count)*100 ;
			return view("score",compact("total","avg"));
		}
	
	public function viewtest() {
		$get =  DB::table("qs1")->get();
		return view("viewtest",compact("get"));
	}
	
	public function insert() {
		$test = new qs1;
		$test->questions = Input::get("questions");
		$test->ans1 = Input::get("ans1");
		$test->ans2 = Input::get("ans2");
		$test->ans3 = Input::get("ans3");
		$test->ans4 = Input::get("ans4");
		$test->crt = Input::get("crt");
		$test->save();
		return view("admin-home");
	}

	public function mail() {
		$email = Auth::user()->email;
		$user = Auth::user()->username;
		$count = qs1::count();
		$id = Auth::user()->id;
		$total = DB::table('marks')->select('*')->where('userid','=',$id)->get();
		$avg =  ($total[0]->mark / $count)*100 ;
		$arr = array(
		'email'=>$email,
		'user'=>$user,
		'total' => $total,
		'avg' => $avg);
		Mail::send('scores', ['arr' => $arr,'total' => $total,'avg' => $avg], function($message) use ($arr)
{	
    $message->from('ganeshjayaram97@gmail.com','Ganesh');
	$message->to($arr['email'], $arr['user'])->subject('Welcome to my Bhoologam :p !');
}); 
	return view('score',compact('total','avg'));
	}
	
		public function logout() {
		Auth::logout();
		return Redirect::to('/test1');
}
	
}
