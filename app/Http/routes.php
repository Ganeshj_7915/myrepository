<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {

Route::get('/', function () {
	#return "Hey every1";
    return view('welcome');
});

Route::get('contact', function(){
	return view('contact');
});


/*  This is for creating articles and updating and Editing it.  */

Route::get('articles','articlecontroller@index');
Route::get('articles/create','articlecontroller@create');
Route::post('articlesi','articlecontroller@display');
Route::get('article/{id}','articlecontroller@show');
Route::get('article/{id}/edit','articlecontroller@edit');
Route::get('article/{id}/update','articlecontroller@update');
Route::get('test',function(){
	return view("test");
});


/* This is for blogging purpose. */

Route::post('signup','authcontroller@signupmethod');
Route::post('signin','authcontroller@signinmethod');
Route::post('profile','profilecontroller@createprof');
Route::get('home',array('as'=>'home','before'=>'auth','uses'=>'Homecontroller@getHome'));
Route::get('homepage',array('as'=>'homepage','before'=>'auth','uses'=>'profilecontroller@homepage'));
Route::get('logout',array('as'=>'logout','before'=>'auth','uses'=>'authcontroller@logoutmethod'));

Route::get('newblog',array('as'=>'newblog','uses'=>'blogcontroller@newblog'));
Route::post('postblog',array('as'=>'postblog','uses'=>'blogcontroller@postblogmethod'));
Route::post('posteditblog',array('as'=>'posteditblog','uses'=>'blogcontroller@posteditblog'));
Route::get('readblog/{id}',array('as'=>'readblog','uses'=>'blogcontroller@readblog'));
Route::get('editblog/{id}',array('as'=>'editblog','uses'=>'blogcontroller@editblog'));
Route::get('deleteblog/{id}',array('as'=>'deleteblog','uses'=>'blogcontroller@deleteblog'));
Route::get('deleteblog/deleteit/{id}',array('as'=>'deleteit','uses'=>'blogcontroller@delblog'));


/*  It is for quiz test purpose..  */


Route::get('test1',function(){
	return view("test1");
});
Route::post('homes','testcontroller@Home');
Route::post('insertqs',array('as'=>'insertqs','before'=>'auth','uses'=>'testcontroller@insert'));
Route::post('submit',array('as'=>'submit','before'=>'auth','uses'=>'testcontroller@submit'));
Route::post('homeadmin',array('as'=>'homeadmin','uses'=>'testcontroller@goadmin'));
Route::get('testqs/{id}',array('as'=>'testqs','before'=>'auth','uses'=>'testcontroller@testqs'));
Route::get('submittest',array('as'=>'submittest','before'=>'auth','uses'=>'testcontroller@testmark'));
Route::get('viewtest',array('as'=>'viewtest','before'=>'auth','uses'=>'testcontroller@viewtest'));
Route::get('logouttest',array('as'=>'logouttest','before'=>'auth','uses'=>'testcontroller@logout'));
Route::get('questions',array('as'=>'questions','before'=>'auth','uses'=>'testcontroller@questions'));
Route::get('sendmail',array('as'=>'sendmail','before'=>'auth','uses'=>'testcontroller@mail'));

/*
Route::get('pdf/invoice/{name}',function($name){
        $invoice = \PDF::loadView('score',array('name'=>$name))->render();
		$invoice->save(storage_path().'/upload/genrep1.pdf');
		return $invoice->stream();
});  */

/*  This is some Initial Testing work when i started learning it .  */

Route::get('about', function() {
	$name = "Ganesh";
	$name1 = "Jayaraman";
	return view('about1')->with(['names' => $name , 'names1' => $name1]);
});

Route::get('content',function(){
	$first = "roy";
	$last = "jason";
	return view('child',compact('first','last'));
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

});
