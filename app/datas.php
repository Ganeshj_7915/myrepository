<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\user;

class datas extends Model
{
	protected $table = "datas";
	  protected $fillable = array('writerid','username','title', 'body');
		
	public function getback() {
		return $this->belongsTo("App\user","id");
	}
}
