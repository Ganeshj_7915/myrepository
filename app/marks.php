<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\user;
use App\qs1;

class marks extends Model
{
	protected $table = "marks";
	protected $fillable = array('userid','mark');
	
	public function getback() {
		return $this->belongsTo("App\qs1","id");
	}
	
}
