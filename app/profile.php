<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\user;

class profile extends Model
{
	protected $table = "profile";
	  protected $fillable = array('profid','profpic');
		
	public function getbackprof() {
		return $this->belongsTo("App\user","id");
	}
	
}
