<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\user;

class qs1 extends Model
{
	protected $table = "qs1";
	protected $fillable = array('questions','ans1','ans2', 'ans3', 'ans4', 'crt');
		
	public function getback() {
		return $this->belongsTo("App\user","id");
	}
}
