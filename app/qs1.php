<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\user;
use App\marks;

class qs1 extends Model
{
	protected $table = "qs1";
	protected $fillable = array('questions','ans1','ans2', 'ans3', 'ans4', 'crt');
	
	public function getmarks() {
		return $this->hasOne("App\marks","userid");
	}
	
}
