<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\datas;
use App\profile;

class user extends  Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = "user";
	
    protected $fillable = [
        'name','username', 'email', 'password',
    ];
	

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
       'password','remember_token',
    ];
	
	public function hasWritten() {
		return $this->hasMany("App\datas","writerid");
	}
	
	public function getprofile() {
		return $this->hasOne("App\profile","profid");
	}
	
}
