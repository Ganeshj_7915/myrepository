$(document).ready(function(){

	$("#signInForm").bootstrapValidator({
		live:'enabled',
		submitButtons:'button[id="signInSubmit"]',
		message:"Not valid ",
		fields:{
			username: {
				validators: {
					notEmpty: {
						 message : "Username is not valid"
					}
				}
			},
			password: {
				validators: {
					notEmpty: {
						 message : "password is not valid"
					},
					stringLength: {
						min: 3,
						max: 18,
						message: "the password should be 3 - 18 characters long"
					}
				}
			}
		}
	});
	
	$("#AdminsignInForm").bootstrapValidator({
		live:'enabled',
		submitButtons:'button[id="adminsignInSubmit"]',
		message:"Not valid ",
		fields:{
			username: {
				validators: {
					notEmpty: {
						 message : "Username is not valid"
					}
				}
			},
			password: {
				validators: {
					notEmpty: {
						 message : "password is not valid"
					},
					stringLength: {
						min: 3,
						max: 18,
						message: "the password should be 3 - 18 characters long"
					}
				}
			}
		}
	});
	
	
	$("#signUpForm").bootstrapValidator({
		live:'enabled',
		submitButtons: 'button[id="signUpSubmit"]',
		message:"Not valid ",
		fields: {
			username: {
				validators: {
					notEmpty: {
						 message : "Username is not valid"
					}
				}
			},
			name: {
				validators: {
					notEmpty: {
						 message : "name is not valid"
					}
				}
			},
			email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    }
                   }
            },
			password: {
				validators: {
					notEmpty: {
						 message : "password is not valid"
					},
					stringLength: {
						min: 3,
						max: 18,
						message: "the password should be 3 - 12 characters long"
					}
				}
			}
		}
	});

});