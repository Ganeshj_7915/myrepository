@extends('apps')

@section('contents')

<h1> Edit : {!! $article->title !!}  </h1>

{!! Form::model($article,['method' => 'PATCH','action' => ['articlecontroller@update', $article->id]]) !!}

    <div class="form-group">

	{!! Form::label("title","enter title here : ") !!}
	{!! Form::text("title",null,["class" => "form-control"]) !!}
	
	
	{!! Form::label("body","body field : ") !!}
	{!! Form::textarea("body",null,["class" => "form-control"]) !!}
	
	{!! Form::label("updated_at","updated at : ") !!}
	{!! Form::input("date","updated_at",null,["class" => "form-control"]) !!}
<br/ >
	
	{!! Form::submit("submit",["class" => "btn btn-primary form-control"]) !!}
	
	</div>
	
{!! Form::close() !!}

@if ($errors->any())
	<ul class="alert alert-danger">
		@foreach ($errors->all() as $error)
			<li />{{ $error }}
		@endforeach
	</ul>
@endif



@stop