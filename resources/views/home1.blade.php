<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<link rel="	stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>
<script src="{{asset('index.js')}}"></script>	

</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myModal" >
<span class="sr-only"> </span>
<span class="icon-bar"> </span>
<span class="icon-bar"> </span>
<span class="icon-bar"> </span>
</button>
<a href="#" class="navbar-brand" style="color:red"> NEW PAGE </a>
</div>

<div class="collapse navbar-collapse" id="myModal">
<ul class="nav navbar-nav navbar-right">
<li><a href="#" data-toggle="modal" data-target="#sign-in-modal">Sign In</a> </li>
<li><a href="#" data-toggle="modal" data-target="#sign-up-modal"> Sign Up </a></li>
</ul>
</div>
</nav>

@if ($errors->any())
	<ul class="alert alert-danger">
		@foreach ($errors->all() as $error)
			<li />{{ $error }}
		@endforeach
	</ul>
@endif




<div class="modal" id="sign-in-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sign In</h4>
      </div>
	  
	 {!! Form::open(array('url' => '/home2','id' => 'signInForm')) !!}
	  
      <div class="modal-body">
	  <fieldset>
	  <div class="form-group col-lg-6 col-lg-offset-3">
	  <input type="text" name="username" id="username" placeholder="username"  class="form-control" autocomplete="off" >
	  </input>
	  </div>
	  
	  <div class="form-group col-lg-6 col-lg-offset-3">
	  <input type="password" name="password" id="password" placeholder="password" class="form-control" autocomplete="off" >
	  </input>
	  </div>
	  </fieldset>
     </div>
	 
      <div class="modal-footer">
        <input type="submit" id="signInSubmit" class="btn btn-primary" value="Submit"></input>
      </div>
	
	{!! Form::close() !!}
	</div>
  </div>
</div>


<div class="modal" id="sign-up-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" > <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sign Up</h4>
      </div>
	  
	  @if ($errors->any())
	<ul class="alert alert-danger">
		@foreach ($errors->all() as $error)
			<li />{{ $error }}
		@endforeach
	</ul>
@endif
	  
	  
	  {!! Form::open(array('url' => '/signup','id' => 'signUpForm')) !!}
	  
      <div class="modal-body">
	  <fieldset>
	  <div class="form-group col-lg-6 col-lg-offset-3">
	  <input type="text" name="name" id="name" placeholder="name"  class="form-control">
	  </input>
	  </div>
	  <div class="form-group col-lg-6 col-lg-offset-3">
	  <input type="text" name="username" id="username" placeholder="username"  class="form-control">
	  </input>
	  </div>
	  
	  <div class="form-group col-lg-6 col-lg-offset-3">
	  <input type="email" name="email" id="email" placeholder="email" class="form-control" >
	  </input>
	  </div>
	  
	  <div class="form-group col-lg-6 col-lg-offset-3">
	  <input type="password" name="password" id="password" placeholder="password" class="form-control" autocomplete="off" >
	  </input>
	  </div>
	  </fieldset>
	  </div>
      <div class="modal-footer">
        <input type="submit" id="signUpSubmit" class="btn btn-primary" value="submit"></input>
      </div>
	 
    </div>
	
	{!! Form::close() !!}
	
	</div>
  </div>

</body>
</html>