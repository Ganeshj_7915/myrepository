<!DOCTYPE html>
<html>
<head>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<link rel="	stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>
<script src="{{asset('newblog.js')}}"></script>	
<script src="{{asset('bootbox.min.js')}}"></script>	
<!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>

<script>

</script>
</head>

<body>
<nav class="navbar navbar-inverse" role="navigation">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myModal" >
<span class="sr-only"> </span>
<span class="icon-bar"> </span>
<span class="icon-bar"> </span>
<span class="icon-bar"> </span>
</button>
<a href="#" class="navbar-brand" style="color:red"> CREATE YOUR PROFILE ..</a>
</div>

<div class="collapse navbar-collapse" id="myModal">
<ul class="nav navbar-nav navbar-right">
<li><a href="logout"> Log Out </a></li>
</ul>
</div>
</nav>

<div class="container">
CREATE YOUR PROFILE HERE :

{!! Form::Open(array('url'=>'profile','method'=>'POST','id' => 'profile','class' => 'form-horizontal'))  !!}

<div class="form-group col-sm-7 col-sm-offset-12">

	<input type="hidden" name="ids" id="ids" value="{{ $user->id }}">
	
	PROFILE PIC. PATH :
	  <input type="text" name="profpic" id="profpic" placeholder="enter path"  class="form-control" autocomplete="off" >
	  </input>
	  </div> 
	  <br />
	  
	  <div class="form-group col-sm-6 col-sm-offset-12">
	  ABOUT YOURSELF  : 
	  <textarea cols="15" rows="10" name="about" id="about" class="form-control summernote" autocomplete="off" >
	  </textarea>
	  </div>
	 <br /><br /><br />
	  
	  <div class="form-group col-sm-12 col-sm-offset-12">
        <input type="submit" id="Submit" class="btn btn-primary" value="Submit"></input>
      </div>
	  

{!! Form::close()  !!}

</div>
</body>

</html>