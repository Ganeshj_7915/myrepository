<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<title> Home Page </title>
<script>
$(document).ready(function(){
	$("#printit").click(function(){
		window.print();
	});
});
</script>
<style>
.test{
	font-size: 1.2em;
	margin : 40px;
}

.answer{
	font-size: 1em;
	margin : 60px;
}
.print{
	margin:10px 500px;
	
}
#printit{
	margin:10px 500px;
}
</style>
</head>
<body>

<div class="container" >
<div class="collapse navbar-collapse" id="myModal">
<ul class="nav navbar-nav navbar-right">
<li><a href="logouttest">Log Out</a> </li>
</ul>
</div>

<h2>Hello {!! Auth::user()->username !!} </h2>


<h3> Your mark for the test is : {{ $total[0]->mark }} </h3>

<h3> Average mark is : {{ $avg }} </h3>

@if ( $avg >= 80.0 )
	<div class="btn btn-success"><b> Great !!  </b></div>
@elseif ( $avg > 50.0 && (80.0 - $avg) > 0  )
	<div class="btn btn-warning"><b>  Ok !! You should also have to Improve a lot !!   </b></div>
@else
	<div class="btn btn-danger"><b> poor !! You should also have to Improve a lot !!  </b></div>
@endif

<center>
<div class="collapse navbar-collapse" id="myModal">
<ul class="nav navbar-nav navbar-left">
<li><div style="margin:10px 500px;"><a href="homes" class="btn btn-success">Take another test</a></div></li>
<li><div id="printit" class="btn btn-success">Print</div></li>
<li><div id="mail" style="margin:10px 500px;" class="btn btn-success"><a href="sendmail">Send Email</a></div></li>
<li><div class="print"><a href="viewtest" class="btn btn-success">View Test</a></div></li>
</ul>
</div>
</center>
	

</body>
</html>

