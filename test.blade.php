<html>
<head>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"></link>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myModal" >
<span class="sr-only"> </span>
<span class="icon-bar"> </span>
<span class="icon-bar"> </span>
<span class="icon-bar"> </span>
</button>
<a href="#" class="navbar-brand" style="color:red"> NEW PAGE </a>
</div>

<div class="collapse navbar-collapse" id="#myModal">
<ul class="nav navbar-nav navbar-right">
<li><a href="#" data-toggle="modal" data-target="#sign-in-modal">Sign In</a> </li>
<li><a href="#" data-toggle="modal" data-target="#sign-up-modal"> Sign Up </a></li>
</ul>
</div>
</nav>


<!-- Modal -->
<div class="modal" id="sign-in-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="sr-only" aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sign In</h4>
      </div>
	  
	  {!! Form::open(array('url' => '/signin','method'=>'POST','id'=>'signInForm')) !!}
	  
      <div class="modal-body">
	  <fieldset>
	  <div class="form-group col-lg-6 col-lg-offset-3">
	  <input type="text" name="username" id="username" placeholder="username" class="form-control">
	  </input>
	  </div>
	  
	  <div class="form-group col-lg-6 col-lg-offset-3">
	  <input type="password" name="password" id="password" placeholder="password" class="form-control">
	  </input>
	  </div>
	  </fieldset>
     
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
	
	{!! Form::close() !!}
	
	</div>
  </div>
</div>

</body>
</html>